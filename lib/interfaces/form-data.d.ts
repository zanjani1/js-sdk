export interface XanoFormData {
    formData: any;
    hasFile: boolean;
    rawFormData: Record<any, any>;
}
//# sourceMappingURL=form-data.d.ts.map