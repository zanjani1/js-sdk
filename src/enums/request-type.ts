export enum XanoRequestType {
    DELETE = 'DELETE',
    GET = 'GET',
    HEAD = 'HEAD',
    PATCH = 'PATCH',
    POST = 'POST',
    PUT = 'PUT',
}
